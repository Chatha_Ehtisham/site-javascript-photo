$(function() {

	
	var imgs = $(".slider li");
	var d1 = $("#d1");
	var d2 = $("#d2");
	var d3 = $("#d3");
	var txt = $(".textS");
	
	
	var rJson = localStorage.getItem("savearticlescreer");
	var articles = JSON.parse(rJson);
	
	var artl= articles.length; 
	if (artl < 3 || artl == "null"){
		alert("Attention il n'y a pas assez d'articles enregistrés pour que le site fonctionne proprement !");
	}
	
	
	$("#img1").append("<img src='"+articles[0].img+"'/>");
	$("#img1").append("<div class='textS' id='txt1'>"+articles[0].titre+"</div>");	
	
	$("#img2").append("<img src='"+articles[1].img+"'/>");	
	$("#img2").append("<div class='textS' id='txt2'>"+articles[1].titre+"</div>");
	
	$("#img3").append("<img src='"+articles[2].img+"'/>");	
	$("#img3").append("<div class='textS' id='txt3'>"+articles[2].titre+"</div>");
	
	
 	imgs.hide();
	txt.hide();

 	var i = 0;

	var nbimg = imgs.length -1;


 	imgs.eq(i).fadeIn(1500);
	txt.eq(i).fadeIn(1500);
 	function nextImage() {

			
			imgs.eq(i).fadeOut(1500);
			txt.eq(i).fadeOut(1500);
			if (i < nbimg){
				i++;
			}
			else{
				i=0;
			}
			imgs.eq(i).fadeIn(1500);
			txt.eq(i).fadeIn(1500);

	}

	function prevImage() {

			
			imgs.eq(i).fadeOut(1500);
			txt.eq(i).fadeOut(1500);
			if (i > 0){
				i--;
			}
			else{
				i = nbimg;
			}
			imgs.eq(i).fadeIn(1500);
			txt.eq(i).fadeIn(1500);
	}

	$(".next").click(function(){
		nextImage();
	})

	$(".previous").click(function()  {
		prevImage();
	})

	d1.click(function(){
			if (i !== 0)
			{
				imgs.eq(i).fadeOut(1500);
				txt.eq(i).fadeOut(1500);
				i=2;
				if (i < nbimg){
					i++;
				}
				else{
					i=0;
				}
				imgs.eq(i).fadeIn(1500);
				txt.eq(i).fadeIn(1500);
			}
	});
	d2.click(function(){
			if (i !== 1)
			{
				imgs.eq(i).fadeOut(1500);
				txt.eq(i).fadeOut(1500);
				i=0;
				if (i < nbimg){
					i++;
				}
				else{
					i=0;
				}
				imgs.eq(i).fadeIn(1500);
				txt.eq(i).fadeIn(1500);
			}
	});
	d3.click(function(){
			if (i !== 2)
			{
				imgs.eq(i).fadeOut(1500);
				txt.eq(i).fadeOut(1500);
				i=1;
				if (i < nbimg){
					i++;
				}
				else{
					i=0;
				}
				imgs.eq(i).fadeIn(1500);
				txt.eq(i).fadeIn(1500);
			}
	});
	
	var timerSlider = setInterval(nextImage, 3000);
	
	$(".slider").hover(function() {
		clearInterval(timerSlider);
	},
	function () {
		timerSlider = setInterval(nextImage, 3000);
	})
	
});